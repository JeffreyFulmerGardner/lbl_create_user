#!/usr/bin/env bash

networksetup -setairportpower aiport off
chmod 775 "_json_parser.py" #Guarantees that the JSON parser can be run as the create user script is most likely run with SU.
string=$(ioreg -l | grep IOPlatformSerialNumber) #This will ask the system for the serial number grep out the information that we need
cutstrings="$(echo $string| cut  -d' ' -f4)" #and then cuts just the serial number out
serialnumber=$(sed -e 's/^"//' -e 's/"$//' <<<"$cutstrings")
token=$1
response=`curl -s https://icusw.lbl.gov/api/imaging-metadata\?serial_number=$serialnumber\&my_ldap=imaging\&token=$token` #Response is the JSON WITH the information that was entered into the system at ICUSW-DEV
error="{\"error\":\"conf not found\"}"

if [ "$response" != " " ]
then
	if [ "$response" != "$error" ]
	then
        SerialNumber=`echo $response | ./_json_parser.py -path "serial_number"` #Serial Number through EndUserFirstNAme uses the Json Parser to extract the nesscary information from the json file
        DoeNumber=`echo $response | ./_json_parser.py -path "doe_number"`
        HostName=`echo $response | ./_json_parser.py -path "hostname"`
        OrgCode=`echo $response | ./_json_parser.py -path "enduser.org_code"`
        BuildingNumber=`echo $response | ./_json_parser.py -path "enduser.building"`
        roomNumber=`echo $response | ./_json_parser.py -path "enduser.room"`
        Location=`echo $response | ./_json_parser.py -path "enduser.org_code"`
        EndUserEmployeeNumber=`echo $response | ./_json_parser.py -path "enduser.employee_id"`
        EndUserWorkPhone=`echo $response | ./_json_parser.py -path "enduser.work_phone"`
        EndUserEmail=`echo $response | ./_json_parser.py -path "enduser.epo"`
        EndUserLastName=`echo $response | ./_json_parser.py -path "enduser.last_name"`
        EndUserFirstName=`echo $response | ./_json_parser.py -path "enduser.first_name"`
        EndUserPassword="!"$EndUserEmployeeNumber
        TechnicianEmployeeNumber=`echo $response | ./_json_parser.py -path "technician.employee_id"`
        TechnicianWorkPhone=`echo $response | ./_json_parser.py -path "technician.work_phone"`
        TechnicianEmail=`echo $response | ./_json_parser.py -path "technician.epo"`
        TechnicianLastName=`echo $response | ./_json_parser.py -path "technician.last_name"`
        TechnicianFirstName=`echo $response | ./_json_parser.py -path "technician.first_name"`
	else
		echo "No Json Detected setting defulat values"  #default values which can be used 
            DoeNumber=1234567
            HostName="Default@lbl.gov"
            OrgCode="Unknown"
            BuildingNumber="Unknown"
            roomNumber="Unknown"
            Location="Unknown"
            EndUserEmployeeNumber="123456"
            EndUserWorkPhone="4151234567"
            EndUserEmail="unknown@lbl.gov"
            EndUserLastName="Doe"
            EndUserFirstName="Jhon"
            EndUserPassword="!"$EndUserEmployeeNumber
            TechEmployeeNumber="123456"
            TechnicianWorkPhone="4151234567"
            TechnicianEmail="Default@lbl.gov"
            TechnicianLastName="Doe"
            TechnicianFirstName="Jane"

	fi
else
	echo "no data in response could be network issue going with defualt values"
            DoeNumber=1234567
            HostName="Default@lbl.gov"
            OrgCode="Unknown"
            BuildingNumber="Unknown"
            roomNumber="Unknown"
            Location="Unknown"
            EndUserEmployeeNumber="123456"
            EndUserWorkPhone="4151234567"
            EndUserEmail="unknown@lbl.gov"
            EndUserLastName="Doe"
            EndUserFirstName="Jhon"
            EndUserPassword="!"$EndUserEmployeeNumber
            TechEmployeeNumber="123456"
            TechnicianWorkPhone="4151234567"
            TechnicianEmail="Default@lbl.gov"
            TechnicianLastName="Doe"
            TechnicianFirstName="Jane"
            
fi


LDAP=`echo $EndUserEmail | cut -d '@' -f 1` #In most cases a persons LDAP is the first half of their email

mkdir -p "/opt/lbl/icusw/imaging"          #creates the directory for the imaging information
echo "$response" > "/opt/lbl/icusw/imaging/imaging-metadata.json" 

echo $SerialNumber >"/opt/lbl/icusw/imaging/SerialNumber" #Serial Number
echo $TechnicianEmployeeNumber >"/opt/lbl/icusw/imaging/TechEmployeeNumber" #Technician Number
echo $DoeNumber >"/opt/lbl/icusw/imaging/DoeNumber" #DoeNumber

EndUserFullName=$EndUserFirstName" "$EndUserLastName #Combines first name and last name in order to fill out the realname field
RealName=$EndUserFullName
UserName=$EndUserFirstName
 
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -computerinfo -set1 -set2 -set3 -set4 -1 $DoeNumber -2 $OrgCode -3 $EndUserEmail -4 $EndUserWorkPhone #This fills in the information in the remote Computermangment fields
#this whole section creates the user account. This is nesscary to be created unlike the PCTECH account because
#we want to create a Profile with Custom fields
sudo dscl . -create /Users/$LDAP
sudo dscl . -create /Users/$LDAP UserShell /bin/bash
sudo dscl . -create /Users/$LDAP RealName $RealName
sudo dscl . -create /Users/$LDAP UniqueID 1004
sudo dscl . -create /Users/$LDAP PrimaryGroupID 1000
sudo dscl . -create /Users/$LDAP NFSHomeDirectory /Users/$LDAP
sudo dscl . -passwd /Users/$LDAP $EndUserPassword
sudo dscl . -append /Groups/admin GroupMembership $LDAP



sudo /usr/sbin/diskutil rename "Macintosh HD" "${LDAP}_HD" #changes the Default Macintosh HD to the Customers LDAP

#sudo mkdir "${RealName}/Users/Shared"

sudo defaults write /Library/Preferences/com.apple.alf globalstate -int 1 #turns on the fire wall

#the next three comands correctly changes the host name sett ings
sudo scutil --set LocalHostName "${HostName}.lbl.gov"
sudo scutil --set HostName "${HostName}.lbl.gov"
sudo scutil --set ComputerName "${HostName}" ### change the rest of the host name settings


defaults write -g CSUIDisable32BitWarning -boolean TRUE # used to disable 32bit warning
#install FireFox
url="https://download.mozilla.org/?product=firefox-latest-ssl&os=osx&lang=en-US"
curl -L -o firefox_installer.dmg $url
cp firefox_installer.dmg  "/Users/Shared"
mkdir firefox_installer
hdiutil attach firefox_installer.dmg -mountpoint firefox_installer -nobrowse
rm -rf /Applications/Firefox.app
cp -r firefox_installer/Firefox.app /Applications/Firefox.app
hdiutil detach firefox_installer
rm firefox_installer.dmg
rm -rf firefox_installer

defaults write -g CSUIDisable32BitWarning -boolean TRUE
#install chrome
url="http://setup.lbl.gov/files/support/googlechrome.dmg"
dmg="chrome.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp chrome.dmg  "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Install
cp -r "./$dir/Google Chrome.app" "/Applications/Google Chrome.app"
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir



defaults write -g CSUIDisable32BitWarning -boolean TRUE
#install sophos
url="http://setup.lbl.gov/files/support/LBL%20Unmanaged%20Sophos.dmg"
dmg="sophos_installer.dmg"
dir=`echo $dmg | cut -d '.' -f 1`

curl -L -o $dmg $url
cp sophos_installer.dmg  "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
#/Library/Application\ Support/Sophos/opm-sa/Installer.app/Contents/MacOS/tools/InstallationDeployer --remove
./$dir/Sophos\ Installer.app/Contents/MacOS/tools/InstallationDeployer --install
hdiutil detach $dir
rm $dmg
rm -rf $dir





defaults write -g CSUIDisable32BitWarning -boolean TRUE

#BigFix
url="https://setup.lbl.gov/files/support/LBL%20BigFix%209.5.5.196-r1.dmg"
dmg="bigfix.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp bigfix.dmg "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Install new
installer -allowUntrusted -pkg ./$dir/*.pkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir


defaults write -g CSUIDisable32BitWarning -boolean TRUE
#install Cisco
url="http://setup.lbl.gov/files/support/anyconnect-macosx-i386-4.3.05017-k9-LBL.dmg"
dmg="anyconnect.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp anyconnect.dmg "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
/opt/cisco/anyconnect/bin/vpn_uninstall.sh
# Install new
installer -pkg ./$dir/*.pkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir



defaults write -g CSUIDisable32BitWarning -boolean TRUE

#install Druva
url="http://setup.lbl.gov/files/support/inSync-5.9.5-r58373.dmg"
dmg="druva.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp druva.dmg "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
# Remove old
# Not sure how to remove
# Install new
installer -pkg ./$dir/*.mpkg -target /
# Cleanup
hdiutil detach $dir
rm $dmg
rm -rf $dir



#Toshiba pritner

url="http://setup.lbl.gov/files/support/TOSHIBA_ColorMFP.dmg"
dmg="Toshiba.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp Toshiba.dmg "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
#open $dir
/usr/sbin/installer -pkg "./$dir/TOSHIBA ColorMFP.pkg" -target "/" -allowUntrusted
hdiutil detach $dir
rm $dmg
rm -rf $dir

#HP printer
url="http://setup.lbl.gov/files/support/HewlettPackardPrinterDrivers5.1.dmg"
dmg="HP.dmg"
dir=`echo $dmg | cut -d '.' -f 1`
# Get
curl -L -o $dmg $url
cp HP.dmg "/Users/Shared"
mkdir $dir
hdiutil attach $dmg -mountpoint $dir -nobrowse
#open $dir
/usr/sbin/installer -pkg "./$dir/HewlettPackardPrinterDrivers.pkg" -target "/" -allowUntrusted
hdiutil detach $dir
rm $dmg
rm -rf $dirsplitDescrpiton = temp.slpit


#creates the new PCTECH Password and then changes it
PCTECHPASSWORD=\!$DoeNumber"Ods"
sudo dscl . -passwd /Users/PCTECH $PCTECHPASSWORD
sudo softwareupdate -r 